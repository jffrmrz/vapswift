//
//  Action.swift
//  VAPS
//
//  Created by Jeff Ramirez on 09/11/2017.
//  Copyright © 2017 Jeff Ramirez. All rights reserved.
//

import Foundation

public protocol Action {
    associatedtype S: State
}
