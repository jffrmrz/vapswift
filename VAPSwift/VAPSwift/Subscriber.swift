//
//  Subscriber.swift
//  VAPSwift
//
//  Created by Jeff Ramirez on 10/11/2017.
//  Copyright © 2017 Jeff Ramirez. All rights reserved.
//

import Foundation

public protocol Subscriber: class {
    var subscribedStates: [State.Type] { get }
    func stateUpdated(state: State)
}
