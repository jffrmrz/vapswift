//
//  Processor.swift
//  VAPSwift
//
//  Created by Jeff Ramirez on 09/11/2017.
//  Copyright © 2017 Jeff Ramirez. All rights reserved.
//

import Foundation

public protocol ProcessorProtocol: AnyObject {}

open class Processor<S: State>: ProcessorProtocol {
    public init() {}
    open func process<A>(_ action: A, _ state: inout S) where A : Action, A.S == S {
        fatalError()
    }
}
