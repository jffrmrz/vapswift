//
//  Relay.swift
//  VAPSwift
//
//  Created by Jeff Ramirez on 09/11/2017.
//  Copyright © 2017 Jeff Ramirez. All rights reserved.
//

import Foundation

public class Relay {
    public static let main = Relay()
    
    private var processors: [ProcessorProtocol]
    private var states: [State] {
        didSet {
            if oldValue.count < states.count {
                let newState = states.last!
                let newStateType = type(of: newState)
                subscribers.forEach { subscriber in
                    if subscriber.subscribedStates.contains(where: { newStateType == $0 }) {
                        subscriber.stateUpdated(state: newState)
                    }
                }
            }
        }
    }
    private var subscribers: [Subscriber]
    private init() {
        processors = []
        states = []
        subscribers = []
    }
    
    public func subscribe(_ subscriber: Subscriber) {
        if !subscribers.contains(where: { $0 === subscriber }) {
            self.subscribers.append(subscriber)
        }
    }
    
    public func unsubscribe(_ subscriber: Subscriber) {
        if subscribers.contains(where: { $0 === subscriber }) {
            self.subscribers.remove(at: self.subscribers.index(where: { $0 === subscriber })!)
        }
    }
    
    public func addProcessor<S>(_ processor: Processor<S>, forState state: S) where S : State {
        if !processors.contains(where: { $0 === processor }) {
            self.processors.append(processor)
            self.states.append(state)
        }
    }
    
    public func sendAction<A>(_ action: A) where A : Action {
        guard let stateIndex = states.index(where: { $0 is A.S }) else { return }
        var state = states.remove(at: stateIndex) as! A.S
        let processor = processors.first(where: { $0 is Processor<A.S> }) as! Processor<A.S>
        
        processor.process(action, &state)
        states.append(state)
    }
}
