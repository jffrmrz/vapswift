//: Playground - noun: a place where people can play

import UIKit
import VAPSwift

struct State1: State {
    var value1 = 0
}

struct State2: State {
    var value1 = 0
}

struct Action1: Action {
    typealias S = State1
    var value: Int
}

struct Action2: Action {
    typealias S = State2
    var value: Int
}

class Processor1: Processor<State1> {
    override func process<A>(_ action: A, _ state: inout State1) where A : Action, A.S == State1 {
        switch(action) {
        case let action as Action1:
            state.value1 = action.value
        default: break
        }
    }
}

class Processor2: Processor<State2> {
    override func process<A>(_ action: A, _ state: inout State2) where A : Action, A.S == State2 {
        switch(action) {
        case let action as Action2:
            state.value1 = action.value
        default: break
        }
    }
}

class Subscriber1: Subscriber {
    var subscribedStates: [State.Type] {
        return [State1.self]
    }
    func stateUpdated(state: State) {
        switch (state) {
        case let state as State1:
            print("Subscriber1-State1: " + state.value1.description)
        default: break
        }
    }
}

class Subscriber2: Subscriber {
    var subscribedStates: [State.Type] {
        return [State2.self]
    }
    func stateUpdated(state: State) {
        switch (state) {
        case let state as State2:
            print("Subscriber2-State2: " + state.value1.description)
        default: break
        }
    }
}

class Subscriber3: Subscriber {
    var subscribedStates: [State.Type] {
        return [State1.self, State2.self]
    }
    func stateUpdated(state: State) {
        switch (state) {
        case let state as State1:
            print("Subscriber3-State1: " + state.value1.description)
        case let state as State2:
            print("Subscriber3-State2: " + state.value1.description)
        default: break
        }
    }
}

Relay.main.addProcessor(Processor1(), forState: State1())
Relay.main.addProcessor(Processor2(), forState: State2())

let subscriber1 = Subscriber1()
let subscriber2 = Subscriber2()
let subscriber3 = Subscriber3()
Relay.main.subscribe(subscriber1)
Relay.main.subscribe(subscriber2)
Relay.main.subscribe(subscriber3)

Relay.main.sendAction(Action1(value: 1))
Relay.main.sendAction(Action2(value: 1))